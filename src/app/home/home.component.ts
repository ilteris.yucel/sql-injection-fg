import { Component, ElementRef, ViewChild } from '@angular/core';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
  constructor(private homeService: HomeService){
    this.homeService = homeService;
  }
  @ViewChild('output') output: ElementRef;
  async request(protectedStatus: boolean, userNameOrEmail: string, password: string){
    if(protectedStatus){
      this.homeService.protectedAuth(userNameOrEmail, password).subscribe(response => {
        this.output.nativeElement.innerHTML = response.message;
      },
      error => {
        this.output.nativeElement.innerHTML = error.error.message;
      }
      ); 
    }else{
      this.homeService.unprotectedAuth(userNameOrEmail, password).subscribe(response => {
        this.output.nativeElement.innerHTML = response.message;
      },
      error => {
        this.output.nativeElement.innerHTML = error.error.message;
      }
      ); 
    }

  }
}
