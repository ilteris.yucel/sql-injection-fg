import { Injectable } from '@angular/core';
 
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs'; 
 
@Injectable({
  providedIn: 'root'
})
export class HomeService {
 
  baseURL: string = "http://localhost:8080";
 
  constructor(private http: HttpClient) {
  }
 
  protectedAuth(userNameOrEmail: string, password: string): Observable<any> {
    return this.http.post(`${this.baseURL}/protected-auth`,{
      usernameOrEmail: userNameOrEmail,
      password: password
    })
  }

  unprotectedAuth(userNameOrEmail: string, password: string): Observable<any> {
    return this.http.post(`${this.baseURL}/unprotected-auth`,{
      usernameOrEmail: userNameOrEmail,
      password: password
    })
  }
 
}